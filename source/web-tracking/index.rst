.. Lasso Registration Forms documentation master file, created by
   sphinx-quickstart on Mon Apr 23 11:48:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Web Tracking Leads
==================

Lasso provides a way to get registrants into LassoCRM through external websites.

.. toctree::

   	getting-started
   	common-problems