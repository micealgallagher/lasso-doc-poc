.. Lasso Registration Forms documentation master file, created by
   sphinx-quickstart on Mon Apr 23 11:48:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Getting Started
====================================================
We will create a web form that posts

Prerequisites
-------------
Before beginning make sure you have the following values:
 
 * ``ClientID`` - an integer that uniquely identifies a `Client` within LassoCRM
 * ``ProjectID`` - an integer that uniquely identifies a client's `Project` within LassoCRM
 * ``LassoUID`` - an short alpha-numeric identifier for a `Project`


Adding form submissions to LassoCRM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~