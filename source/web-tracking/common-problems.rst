.. Lasso Registration Forms documentation master file, created by
   sphinx-quickstart on Mon Apr 23 11:48:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Common Problems - Webtracking
====================================================

I am using Vue
--------------

Unfortunately Lassos ability to track registrants is hindered by GTM.

We recommend add the Lasso tracking script directly to the HTML using </script> tags

I am using Google Tag Manager (GTM)
-----------------------------------

Lasso modifies the DOM using vanilla JavaScript. These modifications to the DOM are overwritten by Vue.js.

We recommend adding the following code to your PageLoad handler

.. code-block:: javascript

    document.forms[0].guid.value = LassoCRM.tracker.readCookie("ut");


Leads are added to Lasso but I don't see any tracking information
-------------------------------------------------------------------
Typically due to LassoCRM not associating the GUID with a Registrant

Verify that the payload being sent to LassoCRM resembles the following:

.. code-block:: javascript

	...
	guid="CD4F931B-5D07-4672-9T8A-1B65FS1A023D" // without this value LassoCRM will be unable to track
	FirstName=Jane
	LastName=Smith
	...